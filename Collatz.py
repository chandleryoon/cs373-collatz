#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

from typing import IO, List

# ------------
# collatz_read
# ------------

solved = {1: 1}


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    assert(i > 0)
    assert(j < 1000000)
    maxlength = 0
    # Swaps the variables if range was negative
    if j < i:
        temp = i
        i = j
        j = temp
    for x in range(i, j + 1):
        original = x
        count = 0

        while x > 1:
            # Found the x in cache
            if x in solved:
                count += solved.get(x)
                break
            # Simple step through
            if x % 2 == 1:
                x = (3 * x) + 1
                count += 1
            x = x / 2
            count += 1
        solved[original] = count
        if count > maxlength:
            maxlength = count
    return maxlength + 1


# -------------
# collatz_print
# -------------


def collatz_print(
    w: IO[str], i: int, j: int, v: int
) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
