#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import (
    collatz_read,
    collatz_eval,
    collatz_print,
    collatz_solve,
)

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_6(self):
        v = collatz_eval(999999, 999998)
        self.assertEqual(v, 259)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 0, 100, 1000000)
        self.assertEqual(w.getvalue(), "0 100 1000000\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1, 1, 20)
        self.assertEqual(w.getvalue(), "1 1 20\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n",
        )

    def test_solve_2(self):
        r = StringIO("8 9\n2 42\n465 353\n2974 4785\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "8 9 20\n2 42 112\n465 353 134\n2974 4785 238\n",
        )

    def test_solve_3(self):
        r = StringIO("2 1\n66666 55555\n235 569\n245 658\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "2 1 2\n66666 55555 335\n235 569 144\n245 658 145\n",
        )

    def test_solve_4(self):
        r = StringIO(
            "63 342\n456 5679\n34988 924988\n23414 99870\n"
        )
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "63 342 144\n456 5679 238\n34988 924988 525\n23414 99870 351\n",
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
